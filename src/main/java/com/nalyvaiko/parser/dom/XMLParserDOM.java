package com.nalyvaiko.parser.dom;

import com.nalyvaiko.model.Bank;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLParserDOM {

  private XMLParserDOM() {
  }

  public static List<Bank> parseXMLBanks(File xmlFile, File xsdFile) {
    List<Bank> banks = new ArrayList<>();
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(xmlFile);
      DOMHandler domHandler = new DOMHandler();
      banks = domHandler.readDOC(document);
    } catch (ParserConfigurationException | IOException | SAXException e) {
      e.getMessage();
    }
    return banks;
  }
}
